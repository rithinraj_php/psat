<?php
session_start();
include("Database/database.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>
 
 
 .legend{
	 font-family: 'Helvetica';
	font-weight: 700;
	color: black;
	font-size: 26px;
	margin-bottom: 10px;
	
}
.customizedPrimaryBtn:hover{
	background: #003635;
	color: white;
	border: none;
}
.customizedPrimaryBtn{
	background: #530;
	color: white;
	border: none;
}
.customizedPrimaryBtn:VISITED{
	background: #066;
	color: white;
	border: none;
}
.customizedPrimaryBtn:ACTIVE{
	background: #003635;
	color: white;
	border: none;
}
 
 
 
   
 </style>
 
 <script>
 
  $(document).ready(function() { 
     document.getElementById("username").focus();
                 
     $("#view_button").bind("mousedown touchstart", function() {
        $("#question").attr("type", "text");
    }), $("#view_button").bind("mouseup touchend", function() {
        $("#question").attr("type", "select");
    });             
                    	  });
 function  valUsername(){
    if(document.getElementById("username").value.trim()==="" && document.getElementById("username").value!==null)
                            	  {
                            	 $('#responseFail').val('');
                            	 $('#username').val('');
                            	
                            	// $("#above").addClass('hidden');
                            	  $('#message').css('color','red');
								  $('#message').html('Please enter username');
								   
								  $('input:text').focus(
										    function(){
										        $(this).css({'border-color' : 'red'});
										        $(this).css({'box-shadow' : 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #f15f5f'});
										    });
								
								  $('input:text').blur(
										    function(){
										    	 $(this).css({'border-color' : '#ccc'});
											        $(this).css({'box-shadow' : 'none'});
											        });
								
								   	$('#username').css({'border-color' : 'red'});
							 $('#username').css({'box-shadow' : 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #f15f5f'});
								  document.getElementById("username").focus();
                            	  return false;
                            	  }
								  else{
								   var name = $('#username').val();
								   if(name=="admin" || name =="Admin" || name=="ADMIN"){
								  //$("#above").removeClass('hidden');
                                       		 $('#message').html('');
                                       		 $('input:text').focus(
         										    function(){
         										        $(this).css({'border-color' : 'red'});
         										        $(this).css({'box-shadow' : 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px #f15f5f'});
         										    });
                                       		 document.getElementById("username").focus();
                                       	$('#responseFail').css({'color' : 'red'});
                                       		$('#responseFail').text('You have entered invalid username');
								 
								   }
								   else{
								       $('#responseFail').css({'color' : '#555'});
								     $('#responseFail').text('select the QUESTION for : '+name);
								 
								          $("#first").addClass('hidden');
                                  //  $("#above").addClass('hidden');
                                    $("#first1").addClass('hidden');
                                    $("#first2").addClass('hidden');
                                    $("#first3").addClass('hidden');//to hide
                                   
                                   // $("#myId").removeClass('hidden');	//to show
                                    $("#myId1").removeClass('hidden');
                                    $("#myId2").removeClass('hidden');
                                    $("#myId3").removeClass('hidden');
                                    $("#myId4").removeClass('hidden');
                                 
                                    $("#myId8").removeClass('hidden');
                                    $('#message').html('');
                                   // $('#message1').html('');
                                   
                                   $('input:select').focus(
										    function(){
										        $(this).css({'border-color' : '#66afe9'});
										        $(this).css({'box-shadow' : 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)'});
										    });
                                   $('input:select').blur(
 										    function(){
 										    	 $(this).css({'border-color' : '#ccc'});
 											        $(this).css({'box-shadow' : 'none'});
 											        });
 											         
                                   $('Response').html('Enter the question for');
                                    document.getElementById("question").focus();
								    }
								  }
         }   
 function prevPage() {
                                	 $('#message').html('');
                                	// $('#message1').html('');
                                	 $('#question').val('');
                                	 $('#responseFail').val('');
                                	
                                 $("#first").removeClass('hidden');
                                 $("#first1").removeClass('hidden');
                                 $("#first2").removeClass('hidden');
                                 $("#first3").removeClass('hidden');//to hide
                                
                                 //$("#myId").addClass('hidden');	//to show
                                 $("#myId1").addClass('hidden');
                                 $("#myId2").addClass('hidden');
                                 $("#myId3").addClass('hidden');
                                 $("#myId4").addClass('hidden');
                             
                                 $("#myId8").addClass('hidden');
                                 $('#ajaxResponse').css('color','#555');
                                 $('input:text').focus(
										    function(){
										        $(this).css({'border-color' : '#66afe9'});
										        $(this).css({'box-shadow' : 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)'});
										    });
                                 $('input:text').blur(
										    function(){
										    	 $(this).css({'border-color' : '#ccc'});
											        $(this).css({'box-shadow' : 'none'});
											        });
                                
                                // document.getElementById("username").blur();// to remove auto focus on usename after back is clicked
                                 document.getElementById("username").focus();
 }
 function loginStatus(){
     alert("This is a demo login application");
     return false;
    
 }
 
 
 </script>
 
 
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  //session_start();
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->



 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
  
  
  <div class="container">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  
  </div>
  
  <div class="container "> 
		<div class=" row">
			<div class="form-group col-md-12">
			<hr/>
			<?php 
				if($_GET){
					
					echo '<label style="text-weight:bold; font-size:20pt; color:red;">'.$_GET['msg'].'</label>';
				}
				?>
				<hr/>
			<form action="fcheck.php" method="post">
				 
				
				<div class="container-fluid" style="margin-top:30px;">
<div class="row">  
<div class="col-md-6 col-md-offset-3" style="padding-right: 0px!important;padding-left: 0px!important;">
    <div class="panel-body" style="padding-right: 4px!important;padding-left: 4px!important;">
<form class="form-horizontal" method="post" id="login" name="login" role="form" onSubmit='#' action="#" AUTOCOMPLETE="off">
	<fieldset  style="min-width: 0;padding:.35em .625em .75em!important;margin:0 2px;border: 2px solid silver!important;margin-bottom: 10em;box-shadow: -6px 15px 20px 0px;">
<legend id="first3" style="width: inherit;padding:inherit;border:2px solid silver;" class="legend">F O R G O T PASSWORD</legend>
<b><p style="color:blue; font-size: 15px;">Reset Your Password</p></b>
				<legend id="myId8" class="hidden legend" style="width: inherit;padding:inherit;border:2px solid silver;">F O R G O T PASSWORD</legend>	
<div class="form-group" id="above" style="margin-bottom: 5px!important;">
									<div class="col-sm-1 col-md-2 col-lg-2 col-xs-1"></div>
									<output class="col-sm-10 col-md-8 col-lg-8 col-xs-10"
										id="responseFail" type="text"
										style="text-align: center; font-weight: bold; color: red;padding: 0px!important;" ></output>
									<div class="col-sm-1 col-md-2 col-lg-2 col-xs-1"></div>
								</div>	
 <div class="form-group" style="margin-bottom: 5px!important;">
                     <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" id="message" style="font-weight: bold; text-align: center;font-size: 10pt;">
						</div>
						 </div>
	<div class="form-group" id="first" style="margin-bottom: 0px!important;">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div
										class="col-sm-10 col-md-10 col-lg-10 col-xs-10 input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-user" style="color: black;"></span></span> <input type="email" name="username"
											class="form-control" id="username" placeholder="Enter Your Email Address">
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>
<div class="form-group" id="first2">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div class="col-sm-10 col-md-10 col-lg-10 col-xs-10"
										style="text-align: right;padding-right: 0px;">
										
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>	
							 
							 
							 	
<div class="form-group" id="first1">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div class="col-sm-11 col-md-11 col-lg-11 col-xs-10" style="text-align:center;">
										<button id="valuser" type="button" onclick="valUsername()"
											class="btn btn-primary customizedPrimaryBtn">
											Next</button>
											
											<a href="loginpage.php"
											<button type="button"
											class="btn btn-primary customizedPrimaryBtn">
											Back</button></a>
									</div>

									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>
								

<div class="form-group hidden" id="myId1" style="margin-bottom: 0px!important;">
									
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div
										class="col-sm-10 col-md-10 col-lg-10 col-xs-10 input-group">
										<span class="input-group-addon"><span
											class="glyphicon glyphicon-question-sign" style="color: black;"></span></span>
											<select  class="form-control" name="question" id="question" required>
		  <option  value="">Select a Question</option>
		  <option> Where were you born?</option>
<option> Which is your favourite place?</option>
<option> What is your favorite food?</option>
<option> Who is your best friend?</option>
		  </select> 
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>	

<div class="form-group hidden" id="myId4">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div class="col-sm-10 col-md-10 col-lg-10 col-xs-10"
										style="text-align: right;padding-right: 0px;">
										
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>	
<div class="form-group hidden" id="myId3">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div class="col-sm-11 col-md-11 col-lg-11 col-xs-10 button_Pad" style="text-align:center">

                                         <button id="submitbtn" type="submit"
											class="btn btn-success"
											name="login"
											style=" font-size: 13px;">
											Next</button>									

					                    <button type="button" onclick="prevPage()"
											class="btn btn-primary"
											style=" font-size: 13px;">
											Back</button>
					
									</div>
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>
							
</fieldset>
</form>
</div>
</div>
</div>
</div>	
			
		</form>
			
		</div> 
	</div>
  
  </div>

	<hr/>
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5">
 </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a></li>
		<li><a href="about.php"> About Us</a></li>
	</ul>
 
 </div>
 </div>

 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
