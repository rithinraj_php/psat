<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

 
 <style>

		.content-header{
  font-family: 'Oleo Script', cursive;
  color:#fcc500;
  font-size: 45px;
}

.section-content{
  text-align: center; 
 background: #3a6186; /* fallback for old browsers */
  background: -webkit-linear-gradient(to left, #3a6186 , #89253e); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to left, #3a6186 , #89253e); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    color : #fff;  
}
#contact{
    
    font-family: 'Teko', sans-serif;
  padding-top: 60px;
  width: 100%;
  width: 100vw;
  height: 550px;
   
}
.contact-section{
  padding-top: 40px;
}
.contact-section .col-md-6{
  width: 50%;
}

.form-line{
  border-right: 1px solid #B29999;
}

.form-group{
  margin-top: 10px;
}
label{
  font-size: 1.3em;
  line-height: 1em;
  font-weight: normal;
}
.form-control{
  font-size: 1.3em;
  color: #080808;
}
textarea.form-control {
    height: 135px;
   /* margin-top: px;*/
}

.submit{
  font-size: 1.1em;
  float: right;
  width: 150px;
  background-color: transparent;
  color: #fff;

}


</style>

 
 
 
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  session_start();
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href='userprofile.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id="" "> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right"  >';
		echo ' <li><a href="loginpage.php" style="text-decoration:none;"><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>';
		echo '<li><a href="coursereg.php" style="text-decoration:none;"><span class="glyphicon glyphicon-user"></span>SignUp</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End--> 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course<span class="caret"></span></a>
          <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
  
  <section id="contact">
			<div class="section-content">
				<h1 class="section-header">Get in <span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> Touch with us</span></h1>
				<h3>Get to Know about any kind of information</h3>
			</div>
			<div class="contact-section">
			<div class="container">
			<hr><?php 
				if($_GET){
					
					echo '<label>'.$_GET['msg'].'</label>';
				}
				?> 
				<hr>
				<form action="contactdb.php" method="post">
					<div class="col-md-6 form-line">
			  			<div class="form-group">
			  				<label for="exampleInputUsername">Your name</label>
					    	<input type="text" class="form-control" id="exampleInputUsername" placeholder=" Enter Name" name="name" required>
				  		</div>
				  		<div class="form-group">
					    	<label for="exampleInputEmail">Email Address</label>
					    	<input type="email" class="form-control" id="exampleInputEmail" placeholder=" Enter Email id" name="mail" required>
					  	</div>	
					  	<div class="form-group">
					    	<label for="telephone">Mobile No.</label>
					    	<input type="tel" class="form-control" pattern="[7-9][0-9]{9}" maxlength="10" title="Enter a valid mobile number" id="telephone" placeholder=" Enter 10-digit mobile no." name="phone" required>
			  			</div>
			  		</div>
			  		<div class="col-md-6">
			  			<div class="form-group">
			  				<label for ="description"> Message</label>
			  			 	<textarea  class="form-control" id="description" placeholder="Enter Your Message" name="query" required></textarea>
			  			</div>
			  			<div>
               <input class="btn btn-warning form-control"  type="submit" value="Send Message" name="submit">
			  			</div>
			  			
					</div>
				</form>
			</div>

		</div>
	</section>
  
  
  
  
   
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a></li>
		<li><a href="about.php"> About Us</a></li>
	</ul>
 
 </div>
 </div>
 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
