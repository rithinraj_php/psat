
<?php
include("Database/database.php");
?>

<?php
session_start();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>
 
 
 @media only screen and (max-device-width:540px) {
    	   .mobileLabel{
   text-align: left;
   }
   	 .mobilePad{
   margin-left: 4em;
   }
}
@media only screen and (max-device-width:750px) and
	(orientation:landscape) {
.mobileLabel{
   text-align: left;
   }
    .mobilePad{
   margin-left: 11%;
   }
	}
		.boxStyle{
margin-left: 20%;width: 60%;
}

 
   
 </style>
 
 
 <script type="text/javascript">
function valid()
{
if(document.form.ans.value=="")
{
alert("Answer Filed is Empty !!");
document.form.ans.focus();
return false;
}
else if(document.form.npass.value=="")
{
alert("New Password Filed is Empty !!");
document.form.npass.focus();
return false;
}
else if(document.form.cpass.value=="")
{
alert("Confirm Password Filed is Empty !!");
document.form.cpass.focus();
return false;
}
else if(document.form.npass.value!= document.form.cpass.value)
{
alert("Password and Confirm Password Field do not match !!");
document.form.cpass.focus();
return false;
}
return true;
}
</script>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
 
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="#"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="#"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
	
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->



 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="#"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="#">MBA</a></li>
          <li><a href="#">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="#">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="#">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
  
  
 
  
  
  			
  
  <div class="container">
	<div class="row">
	
	 
		<div class="col-md-6 col-md-offset-3 boxStyle" style="padding-right: 0px!important;padding-left: 0px!important;">
		   <div class="panel-body" style="padding-right: 4px!important;padding-left: 4px!important;">
                 <form class="form-horizontal" role="form" action="resetcheck.php" method="post" onsubmit="return valid()"name="form">
				<fieldset class="landscape_nomargin" style="min-width: 0;padding:    .35em .625em .75em!important;margin:0 2px;border: 2px solid silver!important;margin-bottom: 10em;">
			<legend style="border-bottom: none;width: inherit;!important;padding:inherit;" class="legend">Reset Password</legend>
		
			<div class="form-group">
								
						 <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" style="text-align: right!important;">
						 <span style="color: red">*</span> <span style="font-size: 8pt;">mandatory fields</span>
						 </div>
						</div>	
						
						<div><?php if($_GET){
		  
		  if($_GET['msg']=="Given Answer is wrong!")
		  {
		  echo '<span style="color:red; font-size:10pt;">'.$_GET['msg'].'</span>';
	  }
		 else{
			  
			  
			  echo '<span style="color:green; font-size:10pt;">'.$_GET['msg'].'</span>';
			  
		  }
		  
	} 
		  ?>
		</div>
			 <div class="form-group" style="margin-bottom: 0px;">
                    <div class="col-sm-4 col-md-4 col-lg-5 col-xs-1"></div><div class="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message10" style=" font-size: 10pt;padding-left: 0px;"></div>                      

                    </div>				
		 <div class="form-group">
                     <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                       <div class="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style=" padding-top: 7px; text-align: right;">
                           User Answer <span style="color: red">*</span> :</div>
                        
						<div class="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad" style="font-weight:600;">
						
						<input style="border-radius: 4px!important;" type="password" placeholder="User answer"  class="form-control" name="ans" required title="Enter your Answer" >                   
                                         
                        </div>
                       <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                    </div>
					
					<div class="form-group">
                     <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                       <div class="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style=" padding-top: 7px; text-align: right;">
                            New password <span style="color: red">*</span> :</div>
                        
						<div class="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad" style="font-weight:600;">
						
						<input style="border-radius: 4px!important;" type="password" class="form-control" name="npass" required title="Enter New password" >                   
                                         
                        </div>
                       <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                    </div>
					
					<div class="form-group">
                     <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                       <div class="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style=" padding-top: 7px; text-align: right;">
                            Confirm password <span style="color: red">*</span> :</div>
                        
						<div class="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad" style="font-weight:600;">
						
						<input style="border-radius: 4px!important;" type="password"  class="form-control" name="cpass" id="cpass" required title="Enter Confirm password" >                   
                                         
                        </div>
                       <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
                    </div>
					
					 
        <div class="form-group">
									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
									<div class="col-sm-11 col-md-11 col-lg-11 col-xs-10" style="text-align:center;">
										<button type="submit" 
											class="btn btn-success" name="submit">
											Submit</button>
											
											<a href="sessionout.php"
											<button type="button"
											class="btn btn-default">
											Login</button></a>
									</div>

									<div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"></div>
								</div>   
			
			</fieldset>
		
				</form>
                </div>
		    </div>
		    
	</div>
</div>
    
<hr/>
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5">
 </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="#">Home</a></li>
		<li><a href="#"> Contact</a></li>
		<li><a href="#"> About Us</a></li>
	</ul>
 
 </div>
 </div>

 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>

