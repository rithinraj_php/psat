<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>

 </style>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  session_start();
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a  href='userprofile.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
	
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->




 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
   <div class="well">
    <h2>MBA Course Details</h2>
  </div>
  
  
  <div class="container">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  
  </div>
  <div class="jumbotron container">
  <div class="row">
		<div class="col-lg-6 ">
		 <img style="float:right;" class="img-thumbnail"  src="images/mba_specialization.png" alt="img">
		</div>
		<div class="col-lg-6" >
		 <p style="font-size:15px"> Presidency B-School is affiliated to Bangalore University and approved by AICTE,
		 New Delhi was established in the year 2002. The Management education at 
		 Presidency B-School is a professional experience that prepares the students for the future.
		 At Presidency, industry interface is a year-round activity with projects, industrial visits and training.</p>
		 
		 <p style="font-size:15px">
		 In addition, Knowledge series by eminent industry personalities keep students
		 well abreast of latest developments in the corporate world. The Presidency 
		 Business School of Presidency College is all set to take the challenges of the new
		 business environment head on and to emerge as an institution of high standing.
		 Presidency B-School is listed in top 100 ranking.
		 </p>
		 <p style="font-size:15px">
		 Presidency B-Schools has been tied up with top Universities such as University of
		 Bedfordshire of UK, La Trobe University of Australia, New College of Manchester of UK.
		 </p>
		 
		 <p style="font-size:15px">The entire course is outcome based and faculty are sensitive to ensure that the outcomes
		 for each course is defined well at the beginning of the programme and shared with each student
		 through course hand outs. This ensures expectation setting is taken care of between the learner 
		 and the teacher. Interactive lectures, pedagogy that is evolving, assignments and assessments 
		 ensure that all students achieve the specified outcomes. The role of the faculty at Presidency 
		 thus is more of a mentor, facilitator and learning is outcome based.
		 </p>
		</div>

  </div>
  <hr>
  </div>
  
  <div class="row ">
  <div class="container jumbotron">
  <div class="col-lg-7">
  <h5 style="font-weight:bold;"> ELIGIBILITY</h5>
  <p style="font-size:15px;">
  The eligibility for MBA under Bangalore University is a minimum of 50% 
  (45 % in case of SC/ST for candidates belonging to Karnataka) aggregate
  in graduation from recognized University or College. The candidate needs
  to have cleared the K-MAT exam to be eligible to apply for Bangalore University MBA.</p>
  
  
   <h5 style="font-weight:bold;">THE SPECIALIZATIONS OFFERED FROM BANGALORE UNIVERSITY INCLUDES:</h5>
   <ul class="mbaspecialization">
	<li class=" glyphicon glyphicon-arrow-right"> Marketing</li>
	<li class="glyphicon glyphicon-arrow-right"> Finance</li>
	<li class="glyphicon glyphicon-arrow-right"> Human Resource Management</li>
	<li class="glyphicon glyphicon-arrow-right"> Information Techolohy</li>
   </ul>
 
  </div>
  <br>
  <div class="col-md-5">
  <form action="coursereg.php" method="post">
  <input class="btn btn-warning" type="submit" value="NEW USERS CLICK HERE TO REGISTER"	>
  </form>
  <hr/>
 
  <form action="loginpage.php" method="">
  <input class="btn btn-success" type="submit" value="REGISTERD USERS CLICK HERE">
  </form>
  <hr>
  
  <form action="mca_test.php" method="">
  <input class="btn btn-primary" type="submit" value="TAKE UP A TEST">
  </form>
  <hr>
  </div>
  </div>
  <hr>
   <div class="container">
  <div class="">
  <img src="images/guidlens-steps.jpg" alt="img"class="img-thumbnail">
  </div>
  </div>
  
 
  
  
  <hr class="">
  
  
  
  <hr class="">
  </div>
  
  <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a></li>
		<li><a href="about.php"> About Us</a></li>
	</ul>
 
 </div>
 </div>
 </div>
</footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
