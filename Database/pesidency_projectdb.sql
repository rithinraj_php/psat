-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2017 at 02:35 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pesidency_projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm`
--

CREATE TABLE `adm` (
  `Admission_ID` int(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phoneno` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `cob` varchar(50) NOT NULL,
  `national` varchar(50) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `cast` varchar(50) NOT NULL,
  `father` varchar(50) NOT NULL,
  `mother` varchar(50) NOT NULL,
  `guard` varchar(50) NOT NULL,
  `no1` varchar(50) NOT NULL,
  `no2` varchar(50) NOT NULL,
  `focc` varchar(50) NOT NULL,
  `mocc` varchar(50) NOT NULL,
  `occ` varchar(50) NOT NULL,
  `pcn` varchar(50) NOT NULL,
  `pc` varchar(50) NOT NULL,
  `per` varchar(50) NOT NULL,
  `yoj` varchar(50) NOT NULL,
  `course` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adm`
--

INSERT INTO `adm` (`Admission_ID`, `student_id`, `name`, `mname`, `lname`, `phoneno`, `email`, `dob`, `cob`, `national`, `religion`, `cast`, `father`, `mother`, `guard`, `no1`, `no2`, `focc`, `mocc`, `occ`, `pcn`, `pc`, `per`, `yoj`, `course`) VALUES
(10004, 5170, 'Rithin ', '', 'kp', '8799562661', 'rithin@gmail.com', '2017-11-02 00:00:00', 'ddvdsf', 'edfaefef', 'efefjg', 'vhjb', 'hbhj', 'bhbhk', 'bhbh', 'bhb', 'hb', 'hb', 'bh', 'b', 'bh', 'kbb', 'kb', 'k', 'MCA'),
(10008, 5166, 'suresh', '', 'P', '8123960069', 'suri@gmail.com', '1995-05-01 00:00:00', 'Sindhnur', 'Indian', 'Hindu', 'kamma', 'Rama Babu P', 'Gangan Bhavani', 'Ganesh B', '9738707080', '8904600225', 'Farmer', 'Home Maker', 'IT Officer', 'Sambhram', 'BCA', '62', '2015', 'MCA');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `pass`) VALUES
(247401, 'Admin', '123'),
(247402, 'Rithin', '111');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`) VALUES
(1, 'MCA'),
(2, 'MBA');

-- --------------------------------------------------------

--
-- Table structure for table `counter`
--

CREATE TABLE `counter` (
  `counter` int(9) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counter`
--

INSERT INTO `counter` (`counter`) VALUES
(12);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `slno` int(11) NOT NULL,
  `stname` varchar(25) NOT NULL,
  `stemail` varchar(50) NOT NULL,
  `stmobileno` varchar(12) NOT NULL,
  `stfeedback` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`slno`, `stname`, `stemail`, `stmobileno`, `stfeedback`) VALUES
(1, 'Mahesh', 'mahesh@gmail.com', '8792007225', 'Hello'),
(2, 'Mahesh', 'mahesh@gmail.com', '8792007225', 'Hello'),
(3, 'Suresh', 'suri@gmail.com', '8123960069', 'Good One!!'),
(4, 'raj', 'raj@gmail.com', '8792226264', 'info'),
(5, 'Tom', 'tom@gmail.com', '7891236542', 'connection'),
(6, 'Ganesh', 'gannu@gmail.com', '8798448895', 'update'),
(7, 'huhhh', 'jjjhj@gmail.com', '7646465664', '5454545454');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(100) NOT NULL,
  `ans1` varchar(80) NOT NULL,
  `ans2` varchar(80) NOT NULL,
  `ans3` varchar(80) NOT NULL,
  `ans4` varchar(80) NOT NULL,
  `ans` int(4) NOT NULL,
  `cat_id` int(4) NOT NULL,
  `answer` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `ans1`, `ans2`, `ans3`, `ans4`, `ans`, `cat_id`, `answer`) VALUES
(1, ' What does PHP stand for?', 'PHP: Hypertext Preprocessor', 'Private Home Page', 'Personal Hypertext Processor', 'none of them', 0, 1, ''),
(2, 'How do you write "Hello World" in PHP', 'echo "Hello World";', '"Hello World";', ' Document.Write("Hello World");', 'none of them', 0, 1, ''),
(3, ' The PHP syntax is most similar to:', 'Perl and C', 'VBScript', 'JavaScript', 'none of them', 0, 1, ''),
(4, 'When using the POST method, variables are displayed in the URL:', 'true', 'false', 'none of them', 'return 1', 1, 1, ''),
(5, 'PHP server scripts are surrounded by delimiters, which?', '&lt;?php...?&gt;', '&lt;?php&gt;...&lt;/?&gt;', '&lt;&amp;&gt;...&lt;/&amp;&gt;', '&lt;script&gt;...&lt;/script&gt;', 0, 1, ''),
(6, 'All variables in PHP start with which symbol?', '@', '%', '$', '&amp;', 2, 1, ''),
(8, 'What is the correct way to end a PHP statement?', '.', ';', 'new line', '&lt;php&gt;', 1, 1, ''),
(10, 'X divided by 899 gives a remainder 63. What remainder will be obtained by dividing X by 29?', '10056', '10001', '10056 ', '10057', 3, 2, ''),
(12, 'What is the full form of SQL?', 'Structured Query Language', 'Structured Query List', 'Simple Query Language  ', 'None of these', 0, 4, ''),
(20, 'a', 'a', 'Structured Query List', 'Simple Query Language  ', 'd', 0, 8, ''),
(21, 'a', 'a', 'Structured Query List', 'Simple Query Language  ', 'd', 0, 8, ''),
(22, 'a', 'a', 'Structured Query List', 'Simple Query Language  ', 'd', 0, 8, ''),
(23, 'ggg', 'asfa', 'asfas', 'asdf', 'fdf', 3, 8, ''),
(24, 'qqqqq', 'Structured Query Language', 'Structured Query List', 'Simple Query Language  ', 'None of these', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `test_id` int(11) NOT NULL,
  `student_id` int(20) NOT NULL,
  `std_course` int(20) NOT NULL,
  `std_cor_ans` int(20) NOT NULL,
  `std_wrng_ans` int(20) NOT NULL,
  `std_percent` int(11) NOT NULL,
  `std_grade` varchar(10) NOT NULL,
  `test_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`test_id`, `student_id`, `std_course`, `std_cor_ans`, `std_wrng_ans`, `std_percent`, `std_grade`, `test_datetime`) VALUES
(18, 5165, 1, 7, 0, 100, 'Pass', '2017-11-13 00:00:00'),
(36, 5166, 1, 7, 0, 100, 'Pass', '2017-11-18 00:00:00'),
(37, 5171, 1, 6, 0, 86, 'Pass', '2017-11-20 00:00:00'),
(38, 5172, 1, 5, 0, 71, 'Pass', '2017-11-20 00:00:00'),
(39, 5173, 1, 1, 0, 14, 'Fail', '2017-11-21 00:00:00'),
(40, 5177, 1, 5, 1, 71, 'Pass', '2017-11-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `student_id` int(8) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `DOB` datetime NOT NULL,
  `course` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`student_id`, `name`, `lname`, `DOB`, `course`, `email`, `mobile`, `password`) VALUES
(5167, 'mahesh', 'E', '1995-03-02 00:00:00', '1', 'mahi@gmail.com', '8792007225', 'c6f057b86584942e415435ffb1fa93d4'),
(5170, 'Rithin ', 'kp', '2017-11-02 00:00:00', '1', 'rithin@gmail.com', '8799562661', '698d51a19d8a121ce581499d7b701668'),
(5173, 'tom', 'sam', '2017-11-01 00:00:00', '1', 'tom@gmail.com', '5555555555', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adm`
--
ALTER TABLE `adm`
  ADD PRIMARY KEY (`Admission_ID`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adm`
--
ALTER TABLE `adm`
  MODIFY `Admission_ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10009;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247403;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `student_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5180;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
