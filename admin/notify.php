<?php
session_start();
include("../Database/database.php");
include ("../class/users.php");
$profile=new users;
$profile->show_messages();



		
	  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: loginpage.php");
		  exit;
	  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="bootstrap.min.css" rel="stylesheet" media="screen">
 <style>

			
 
 
 
 
 </style>
 <script>
 
 
 $(document).ready(function() {
    $('#datatable').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

 
 
 </script>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
 
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a  href='index.php'>Welcome ".$_SESSION['ASID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
	
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	
	
	 
	  <div class="col-md-4 nav navbar-nav">
    <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
      <li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
  
  
  <div class="container">
  <div class="row well">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="../images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  </div>
  </div>
  
  
  
  
  <h2>Notifications </h2>
  <hr/>
  
  <div class="container">
	<div class="row">
		
	</div>
    
    <div class="row">
		
        <div class="col-md-12">
            
            
				<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
    				<thead>
						<tr>
							<th>Slno</th>
							<th>Student Name</th>
							<th>Email Id</th>
							<th>Mobile No</th>
							<th>Message</th>
							
							
                                
                                 
						</tr>
					</thead>

					<tfoot>
						<tr>
							<th>Slno</th>
							<th>Student Name</th>
							<th>Email Id</th>
							<th>Mobile No</th>
							<th>Message</th>
							
                             
                                 
						</tr>
					</tfoot>

					<tbody>
					<?php	
							
									
									foreach($profile->msg as $messages)
									
								{  ?>
						<tr>
						
						
							<td><?php echo $messages['slno'];?></td>
							<td><?php echo $messages['stname'];?></td>
							<td><?php echo $messages['stemail'];?></td>
							<td><?php echo $messages['stmobileno'];?></td>
							<td><?php echo $messages['stfeedback'];?></td>
							
          
						 </tr><?php   }?>
					</tbody> 
				</table>
		
		</div>
	</div>
</div>
  
  <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="">Home</a></li>
		<li><a href="">Admin Controls</a></li>
		<li><a href="">Notifications</a></li>
	</ul>
 
 </div>
 </div>
 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
