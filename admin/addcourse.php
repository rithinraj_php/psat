<?php  session_start();

  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: loginpage.php");
		  exit;
	  }


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
	

    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="bootstrap.min.css" rel="stylesheet" media="screen">
 </head>
 
 <style> 
 

 
 </style>

<body>

 
 
 
 
 
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
	 
	  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: admin.php");
		  exit;
	  }
  
	if(!empty($_SESSION['ASID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a  href='index.php'>Welcome ".$_SESSION['ASID']."</a></li>";
		echo  "<li><a  href='adminlogout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo  '<li><a  id="myBtn"><span class="glyphicon glyphicon-log-in"></span> login </a></li>';
		echo '<li><a  id="myBtn2"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog ">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Login</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form  action="login.php" method="post">
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
              <input type="text" class="form-control"  name="username" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control"  name="pass" placeholder="Enter password">
            </div>
            <div class="checkbox">
              <label><input type="checkbox" value="" checked>Remember me</label>
            </div>
			  <span class="glyphicon glyphicon-off"></span>
              <input type="submit" name="login" class="btn btn-success btn-block " value="Login"> 
			  <input type="hidden" name="url" id="redirect_url" value="index.php" />
           </form>
        </div>
        
        <div class="modal-footer">
        
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
          <p>Not a member? <a href="#signup" id="signup">Sign Up</a></p>
          <p>Forgot <a href="#">Password?</a></p>
        </div>
      </div>
     
    </div>
  </div> 

 
<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
 
</script>

				 <!-- END Login model login -->

   <!-- Model signUp -->
   
   <div class="container">
  
  <!-- Trigger the modal with a button -->
  

  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog ">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 70px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Sign Up</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form  method="post" action="register.php">
            <div class="form-group">
              <label for="fname"><span class="glyphicon glyphicon-user"></span> Username</label>
              <input type="text" class="form-control"  name="fname" placeholder="Enter email" required>
            </div>
           
            <div class="form-group">
              <label for="email"><span class="glyphicon glyphicon glyphicon-envelope"></span> E-mail</label>
              <input type="text" class="form-control"  name="email" placeholder="example@mail.com" required>
            </div>
            
            <div class="form-group">
              <label for="pass"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control"  name="pass" placeholder="Enter password" required>
            </div>
            <div class="form-group">
              <label for="address"><span class="glyphicon glyphicon-map-marker"></span> Address</label>
              <input type="text" class="form-control"  name="address" placeholder="Address" required>
            </div>
            
            <div class="form-group">
            <label for="Gender"> <span class="fa fa-venus-double"> <b>Gender</b></span></label>
            <select id="Gender" name="gender" class="form-control" required>
			  <option value="">I am...</option>
			  <option value="FEMALE" >
			  Female</option>
			  <option value="MALE" >
			  Male</option>
			  <option value="OTHER" >
			  Other</option>
			  </select>
            </div>
            <div class="form-group">
              <label for="tel"><span class="glyphicon glyphicon-phone"></span> Contact</label>
              <input type="text" class="form-control"  name="phone" placeholder="Phone" required>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" value="" checked>Remember me</label>
            </div> 
            <button type="submit" class="btn btn-success btn-block "><span class="glyphicon glyphicon-thumbs-up"></span> Sign Up</button>
                
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
          <p>Already member? <a href="#signin" id="signin">Sign In</a></p>
          <p>Forgot <a href="#">Password?</a></p>
        </div>
      </div>
      
    </div>
  </div> 
</div>
 
<script>
$(document).ready(function(){
    $("#signup").click(function(){
        $("#myModal2").modal();
       
    });
    
    
});


$(document).ready(function(){
    $("#myBtn2").click(function(){
        $("#myModal2").modal();
    });
});


</script>

 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	
	  
	 
	  <div class="col-md-4 nav navbar-nav">
      <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
     <li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
 
  </header>
  
  
    <div class="container">
  <div class="row well">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="../images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  </div>
  </div>
  
  
  <div class="container">
  
		<div class="row">
			
			<div class="col-md-3">
				<div class="form-group" >
				<form action="course.php" method="post">
				<hr/><?php 
				if($_GET){
					
					echo '<label>'.$_GET['msg'].'</label>';
				}
				?>
				<hr/>
				<div class="container">
				<div class="col-md-3">
				<div class="row well">
				<label for="course"> Course Name:</label>
				<input class="form-control" id="course"type="text" name="coursename" required> 
				<br>
				<input type="submit" name="submit" class="btn btn-success">
				</form>
				</div>
			</div>
			</div>
			</div>
			</div>
			
		</div>
  
  </div>
<hr/>
  
  


 
 


  
   <hr>
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="">Offred Courses</a</li>
		<li><a href=""> Contact</a</li>
		<li><a href=""> About Us</a</li>
	</ul>
 
 </div>
 </div>
 </div>
</footer>
  
  
  
 
 
<script src="bootstrap.min.js"></script>
</body>

</html>



