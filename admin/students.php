<?php
session_start();
include("../Database/database.php");
include ("../class/users.php");
$profile=new users;
$profile->show_students();


  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: loginpage.php");
		  exit;
	  }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="bootstrap.min.css" rel="stylesheet" media="screen">
 
 <script>
 
 
 $(document).ready(function() {
    $('#datatable').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

 
 
 </script>
 <style>
 
	.pagination>li {
display: inline;
padding:0px !important;
margin:0px !important;
border:none !important;
}
.modal-backdrop {
  z-index: -1 !important;
}
/*
Fix to show in full screen demo
*/
iframe
{
    height:700px !important;
}

.btn {
display: inline-block;
padding: 6px 12px !important;
margin-bottom: 0;
font-size: 14px;
font-weight: 400;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}

.btn-primary {
color: #fff !important;
background: #428bca !important;
border-color: #357ebd !important;
box-shadow:none !important;
}
.btn-danger {
color: #fff !important;
background: #d9534f !important;
border-color: #d9534f !important;
box-shadow:none !important;
}
 
 </style>
 
<body>




  </head>
 
 
 
 
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
 
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href='index.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		//echo 'hello';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>

	  
	 
	  <div class="col-md-4 nav navbar-nav">
      <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
     <li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  
  </header>
  

    <div class="container">
  <div class="row well">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="../images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  </div>
  </div>


<div class="container">
	<div class="row">
		<h2 class="text-center">Students List</h2>
	</div>
    
    <div class="row">
		
        <div class="col-md-12">
            
            
				<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
    				<thead>
						<tr>
							<th>Reg</th>
							<th>Name</th>
							<th>Last Name</th>
							<th>Course</th>
							<th>Mail</th>
							<th>Phone</th>
							
                                
                               
						</tr>
					</thead>

					<tfoot>
						<tr>
							<th>Reg</th>
							<th>Name</th>
							<th>Last Name</th>
							<th>Course</th>
							<th>Mail</th>
							<th>Phone</th>
							
                             
                               
						</tr>
					</tfoot>

					<tbody>
					<?php	
							
									
									foreach($profile->slist as $students)
									
								{  ?>
						<tr>
						       
						 
							<td><?php echo $students['student_id'];?></td>
							<td><?php echo $students['name'];?></td>
							<td><?php echo $students['lname'];?></td>
							<td><?php echo $students['course'];?></td>
							<td><?php echo $students['email'];?></td>
							<td><?php echo $students['mobile'];?></td> 
          
						 </tr><?php   }?>
					</tbody> 
				</table>
		
		</div>
	</div>
</div>
 



 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="">Offred Courses</a</li>
		<li><a href=""> Contact</a</li>
		<li><a href=""> About Us</a</li>
	</ul>
 
 </div>
 </div>
 </div>
 
 
 
 
 
 
 </footer>
<hr/>
 
<script src="js/bootstrap.min.js"></script>
</body>
</html>

