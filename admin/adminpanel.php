<?php
 session_start();
include("../Database/database.php");

  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: loginpage.php");
		  exit;
	  }



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
	

    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="bootstrap.min.css" rel="stylesheet" media="screen">
 </head>
 
 <style> 
 
body { padding-top:20px; }
.panel-body .btn:not(.btn-block) { width:120px; margin-bottom:10px; }
 .dash{
	 margin-left: 350px;
 }
 
 </style>

<body>

 
 
 
 
 
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
	 
	  if((isset($_SESSION['ASID'])) && $_SESSION['ASID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: admin.php");
		  exit;
	  }
  
	if(!empty($_SESSION['ASID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a  href='index.php'>Welcome ".$_SESSION['ASID']."</a></li>";
		echo  "<li><a  href='adminlogout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo  '<li><a  id="myBtn"><span class="glyphicon glyphicon-log-in"></span> login </a></li>';
		echo '<li><a  id="myBtn2"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->

 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	
	  
	 
	  <div class="col-md-4 nav navbar-nav">
     <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
<li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
 
  </header>
  
  <div class="container">
  <div class="row well">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="../images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  </div>
  </div>
 <div class="container dash">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Admin Controls</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                          <a href="addcourse.php" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Add Course</a>
                          <a href="deletecourse.php" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Del Course</a>
                          <a href="addquestions.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Questions</a>
                          <a href="results.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Results</a>
                        </div>
                        <div class="col-xs-6 col-md-6">
                          <a href="addadmin.php" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Add Admin</a>
                          <a href="students.php" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Students</a>
                          <a href="deleteques.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-picture"></span><br/>Del Ques</a>
                          <a href="notify.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Notify</a>
                        </div>
                    </div>
                    <a href="index.php" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span>  Home</a>
                </div>
            </div>
        </div>
    </div>
</div>

 
  <hr>

  
  
  
    <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="adminpanel.php">Admin Controls</a></li>
		<li><a href="notify.php">Notifications</a></li>
	</ul>
 
 </div>
 </div>
 </div>
</footer>
  
  
  
 
 
<script src="bootstrap.min.js"></script>
</body>

</html>

