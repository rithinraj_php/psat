<?php
session_start();
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>

		
 </style>
  </head>
 

 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> 
	  <?php
 
	if(!empty($_SESSION['ASID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"><span style="text-align: center; font:20pt Arial; color:#04223A;"><b>ADMIN PAGE</b></span>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href=''>Admin: <span style='color:red;font-size:20px;'> ".$_SESSION['ASID']."</span></a></li>";
		echo  "<li><a  href='adminlogout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id="" "> ';
		echo '<img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"><span style="text-align: center; font:13pt Arial; color:#04223A;"><b>STUDENT ADMISSION TEST</b></span>';
		echo '<ul class="nav navbar-nav navbar-right"  >';
		echo ' <li><a href="loginpage.php" style="text-decoration:none;"><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> 


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	
	
	 
	  <div class="col-md-4 nav navbar-nav">
    <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
<li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
   
 <div id="clockbox" style="text-align: center; font:14pt Arial; color:#0000ff;"></div> 
  
<br/><br/>
  
  <div class="container">
      
    <div class="carousel-inner">
      <div class="item active">
        <img src="../images/t6.jpg" alt="Los Angeles" style="width:100%;">
      </div>

  </div>
</div>
  
  <br/>
  <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="adminpanel.php">Admin Controls</a></li>
		<li><a href="notify.php">Notifications</a></li>
	</ul>
 
 </div>
 </div>
 </footer>
 
 <script type="text/javascript">
tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

function GetClock(){
var d=new Date();
var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getFullYear();
var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

if(nhour==0){ap=" AM";nhour=12;}
else if(nhour<12){ap=" AM";}
else if(nhour==12){ap=" PM";}
else if(nhour>12){ap=" PM";nhour-=12;}

if(nmin<=9) nmin="0"+nmin;
if(nsec<=9) nsec="0"+nsec;

document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
}

window.onload=function(){
GetClock();
setInterval(GetClock,1000);
}
</script>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
