<?php

 session_start();?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="../images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
	

    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="bootstrap.min.css" rel="stylesheet" media="screen">


<body>

  </head>
 
 
 
 
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  
	if(!empty($_SESSION['ASIDSID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav>


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-4">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	
	  
	 
	  <div class="col-md-4 nav navbar-nav">
      <li> <a class="mnuclr" href="adminpanel.php">Admin Controls </a></li>
	  </div>
	  
	  
	  <div class="col-md-4 nav navbar-nav">
<li><a  class="mnuclr" href="notify.php">Notifications</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  



  </header>
  
  
  
  
  
    <div class="container">
  <div class="row well">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="../images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="../images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  </div>
  </div>
  
  
  
  
  
  
  
  <div class="container "> 
  
		<div class=" row well">
		
			<div class="form-group col-md-3">
			<form action="adminlog.php" method="post">
			<?php 
				if($_GET){
					
					echo '<b>'.$_GET['msg'];
				}
				?>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input id="email" type="text" class="form-control" name="adminusr" placeholder="Username">
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input id="password" type="password" class="form-control" name="adminpass" placeholder="Password">

				</div> 
				<div>
						<br>
					<input class=" btn btn-primary btn-md" type="submit" value="Login"><br>
					<input type="hidden" value="adminpanel.php" name="adminpage">
				</div>
			</form>
			
		</div> 
	</div>
  
  </div>
 
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="">Home</a></li>
		<li><a href="">Admin Controls</a></li>
		<li><a href="">Notifications</a></li>
	</ul>
 
 </div>
 </div>
 
 </footer>
<hr/>
 
<script src="js/bootstrap.min.js"></script>
</body>
</html>

