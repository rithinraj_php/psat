<?php
include("Database/database.php");
?><?php 
session_start();


		
	  if((isset($_SESSION['SID'])) && $_SESSION['SID'] ==true)
	  {
		 
	  }
	  else{
		  header("Location: loginpage.php");
		  exit;
	  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>

			
 
 
 
 
 </style>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
 
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a  href='userprofile.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
	
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
  <div class="container" >
  <div class="well">
 
  <h1 class="page-header">Edit Profile</h1>
  <div class="row">
 	<?php
	
	$sql="select * from user where student_id='".$_SESSION['STUDENTID']."'";
	$res=mysqli_query($con,$sql);
	$count=mysqli_num_rows($res);
	
	if($count>=1){ while($row = mysqli_fetch_array($res)) {
		$sql2="select cat_name from category where id='".$row['course']."'";
		$res1=mysqli_query($con,$sql2);
	$count1=mysqli_num_rows($res1);
	$sql3="select std_percent from results where student_id='".$_SESSION['STUDENTID']."'";
		$res2=mysqli_query($con,$sql3);
		$count2=mysqli_num_rows($res2);
			$sql4="select std_grade from results where student_id='".$_SESSION['STUDENTID']."'";
		$res3=mysqli_query($con,$sql4);
$count3=mysqli_num_rows($res3);
	
		?>
		
	
    <div class="col-md-8 col-sm-6 col-xs-12 personal-info ">
      
      <h3>Personal info  </h3>
	  
	  
		  <form class="form-horizontal" role="form" action="updateusrinfo.php" method="post">
        <div class="form-group">
          <label class="col-lg-3 control-label">First name:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" name="name" value="<?php echo $row['name'];?>" pattern="[A-Za-z]+" required title="only letters">
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-3 control-label">Last name:</label>
          <div class="col-lg-8">
            <input class="form-control"  name="lname" type="text" value="<?php echo $row['lname'];?>" pattern="[A-Za-z]+" required title="only letters">
          </div>
        </div>
		  <div class="form-group">
          <label class="col-lg-3 control-label">DOB:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" disabled="disabled" name="dob" value="<?php echo trim($row['DOB'],"00:00:00");?>">
          </div>
        </div>	
        <div class="form-group">
          <label class="col-lg-3 control-label">Course:</label>
          <div class="col-lg-8">
            <input class="form-control" disabled="disabled"  type="text" value="<?php 
			if ($count1>=1){while($row1 = mysqli_fetch_array($res1)) {echo $row1['cat_name']; $_SESSION['coursename']=$row1['cat_name'];}}?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-3 control-label">Email:</label>
          <div class="col-lg-8">
            <input class="form-control" disabled="disabled"   type="email" name="email" value="<?php echo $row['email'];?>">
          </div>
        </div>
        
		<div class="form-group">
          <label class="col-lg-3 control-label">Phone:</label>
          <div class="col-lg-8">
            <input class="form-control" disabled="disabled" pattern="[7-9][0-9]{9}" maxlength="10"  type="text" name="phone" value="<?php echo $row['mobile'];?>" required>
          </div>
        </div>
		
		<div class="form-group">
          <label class="col-lg-3 control-label">Results:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" disabled="disabled" value="<?php 
			if ($count2>=1){while($row2 = mysqli_fetch_array($res2)) {echo $row2['std_percent'].' %';}}?>">
			
			
          </div>
        </div>
		<div class="form-group">
          <label class="col-lg-3 control-label">Grade Report:</label>
          <div class="col-lg-8">
            <input class="form-control" type="text" disabled="disabled" value="<?php 
			if ($count2>=1){while($row2 = mysqli_fetch_array($res3)) {echo $row2['std_grade'];}}?>">
			
			
          </div>
        </div>
		
        
		
		
        <div class="form-group">
          <label class="col-md-3 control-label">Password:</label>
          <div class="col-md-8">
            <input class="form-control"  type="password" name="pass">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input class="btn btn-primary" value="Save Changes" type="submit" name="update">
            <span></span>
            <input class="btn btn-default" value="Cancel" type="reset">
          </div>
        </div>
      </form>
	  
	<?php }}?>
    </div>
 
 <div class="col-md-3 anchor">
		  <ul class="list-group">
		 
		  <br/>
		  <br/>
		 <li class="list-group-item list-group-item-primary"><?php if($_GET){
		  
		  if($_GET['msg']=="Your profile has been successfully updated.")
		  {
		  echo '<b><span style="color:green; font-size:10pt;">'.$_GET['msg'].'</span></b>';
	  }
		 else{
			  
			  
			  echo '<b><span style="color:red; font-size:10pt;">'.$_GET['msg'].'</span></b>';
			  
		  }
		  
	} 
		  ?></li>
		  <br/>
		  <br/>
		  <a href="mca_test.php"> <li class="list-group-item list-group-item-danger">Test</li></a><br>
          <br>
		  <br>
		  	 
		 <a href="results.php"> <li class="list-group-item list-group-item-success"> Results</li></a><br>
		 <br>
		  <br>
		<a href="admissiondocs.php"> <li class="list-group-item list-group-item-primary">Required Docs for Admission Form</li></a><br>
		<br>
		 <br>
		<a href="admissionform.php"> <li class="list-group-item list-group-item-info">Admission Form</li></a><br>
		<br>
		 <br>
		<a href="cpass.php"> <li class="list-group-item list-group-item-danger">Change Password</li></a><br>
		</ul>
		 </div>


</div>
  
  </div>
   </div>
  <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="course.php">Offred Courses</a</li>
		<<li><a href="contact.php"> Contact</a</li>
		<li><a href="about.php"> About Us</a</li>
	</ul>
 
 </div>
 </div>
 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
