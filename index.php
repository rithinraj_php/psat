
<?php  session_start();?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
	<style type="text/css">
	div.information 
{
    font-family:tahoma;
    color:#33B2FF;
    margin:8px;
    padding:20px;
}	
	</style>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<body> 

   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> 
	  
	  <?php
 
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo "<span id='demo' style='text-align: center; font:15pt Arial; color:#0000ff;'></span>";
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href='userprofile.php'>Welcome: <span style='color:red;font-size:20px;'> ".$_SESSION['SID']."</span></a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id="" "> ';
		echo '<img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"><span style="text-align: center; font:13pt Arial; color:#04223A;"><b>STUDENT ADMISSION TEST</b></span>';
		echo '<ul class="nav navbar-nav navbar-right"  >';
		echo ' <li><a href="loginpage.php" style="text-decoration:none;"><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>';
		echo '<li><a href="coursereg.php" style="text-decoration:none;"><span class="glyphicon glyphicon-user"></span> SignUp</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->

  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  
  </header>
  
  
  <div class="container">
   
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
 
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

  
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/t4.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="images/t1.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="images/t3.jpg" alt="New york" style="width:100%;">
      </div>
    </div>

    
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
  
  
  

<br><br>
<hr>
  
  <div class="container ">
    <div class="row">
  <div class="col-md-6">
  <center><h3><u><strong>Programs that change your Life @ Presidency College</strong></u></h3></center>
  <p >A variety of teaching and learning methods are used to impart knowledge
     and skills of the students, Lecturers, Case Studies, Group Discussion, 
	 Students Presentations, Class Tests, Quizzes, etc are used to 
	 develop conceptual and analytical skill in order to better prepare 
	 the students to face the challenges of the corporate world.</p>
	 </div>
	  <div class="col-md-6">
	 <center><h3><u><strong>Long term planning of Presidency College</strong></u></h3></center>
	 <br>
	 <ul><li>Educational<br/></li>
     <li>Administrative </li>
	<li> Infrastructural</li>
	<li>Leadership and Governance</li><br/><ul>
</div>
</div>
</div>




 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 
 <p style="text-align: center; font:14pt Arial; color:white;" >
 
    <?php 
	if(!empty($_SESSION['SID']))
	{ 
     include 'counter.php'; 
    }
	?>
	 </p>
 
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a</li>
		<li><a href="about.php"> About Us</a</li>
	</ul>
 
 </div>
 </div>
 </div>
 
 </footer>
 
<script>
function myFunction() {
    var greeting;
    var time = new Date().getHours();
    if (time < 10) {
        greeting = "Good morning";
    } else if (time < 20) {
        greeting = "Good day";
    } else {
        greeting = "Good evening";
    }
document.getElementById("demo").innerHTML = greeting;
}

window.onload=function(){
myFunction();
}
</script>

<script src="js/bootstrap.min.js"></script>
</body>
</html>

