<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>
body { 
    background-image: url('bg.png');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position:center; 
	background-size: 30%;
}

</style>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  session_start();
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href='userprofile.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id="" "> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right"  >';
		echo ' <li><a href="loginpage.php" style="text-decoration:none;"><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>';
		echo '<li><a href="coursereg.php" style="text-decoration:none;"><span class="glyphicon glyphicon-user"></span>SignUp</a></li>';
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
    
  
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/highered-banner1.jpg" alt="img">
  </div>
  </div>
     <hr>
  <br>
   <br/>
 
 <br/>
 <hr/>
 <hr/>
  <div class="well">
    <h2>Overview About-Us</h2>
  </div>
  <hr/>
  <div class="container">
  <div class="row">
		
		<div class="col-md-7" >
	   
<img  src="images/index1.jpg" alt="presidency" class="img-thumbnail" align="right" width="100%" height="100%"> 

		</div>
		
		<div class="col-md-5">
		<br/>
		<br/>
		
		 <p style="font-size:18px"><b>Empowering students to recognize & optimize their full potential.<br/><br/>
		 Welcome to Presidency College, an Institution that rests on a strong academic foundation<br/>
                       <center style="font-size:18px">- PRINCIPAL’S MESSAGE</center>
		 </b></p>
		
		</div>
		

  </div>
 
  </div>
   <hr/>
<div class="container">
  <div class="row">
		
		
		<div class="col-md-6">
		<br/>
		<br/>
		<br/>
		 <p style="font-size:18px"><b>Presidency Group of Institution was established in the year 1976.
                                   Close to four decades now the Institution under the hawk’s eye vision 
								   of its Chairman has been continuously working towards the growth
								   approaching several branches and holding proud position in the field of education.
                                     Presidency College was started in the year 2000. 
									 Affiliated to Bangalore University the college started with just two courses. 
									 Today 13 years down the lane we are proud to announce that 
									 Presidency College is a renowned B School with its Management Courses
									 flourishing every year with great results..
		 </b></p>
		
		</div>
	   <div class="col-md-6" >
<img  src="images/Admission.jpg"  class="img-thumbnail" alt="presidency" height="50%" width="100%" align="left"> 

		</div>
		
		

  </div>
 
  </div>
   <hr/>
    <div class="container">
  <div class="row">
		
		<div class="col-md-8" >
	   
<img  src="images/col.jpg" alt="presidency"  class="img-thumbnail" align="right" width="100%" height="100%"> 
<img  src="images/col2.jpg" alt="presidency" class="img-thumbnail" align="right" width="80%" height="100%"> 
		</div>
		
		<div class="col-md-4">
		
		
		 <b><u><h3>VISION:-</h3></u></b><p style="font-size:18px" >
            <b>In a caring & positive environment, Presidency will provide education
			to empower our students to recognize & optimize their full potential,
			to achieve personal standards of excellence in academic work as well 
			as in supportive areas of physical, cultural and social development, 
			inculcating civic and human values.</p>
			<b><u><h3>MISSION:-</h3></u></b>
          <p style="font-size:18px" > <b> To empower our students to recognize & optimize their full potential; 
			by fostering a family environment where educational, social, cultural, 
			ethical & emotional needs are addressed through a holistic program,
		    offered with the partnership afforded by staff, students & community at large, 
		    to provide world-class education.
		 </b></p>
		</div>
  </div>
   </div>
   <hr/>
   <hr/>
   
   
 <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a></li>
		<li><a href="about.php"> About Us</a></li>
	</ul>
 
 </div>
 </div>
 
 
 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>

