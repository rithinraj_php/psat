<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<link rel="shortcut icon" href="images/favicon-32x32.png" type="image/x-icon">
	<link rel="stylesheet" href="stylesheet.css" type="text/css">
    <title>Presidency Admission Portal  </title>
	<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="jquery-ui.css" rel="stylesheet">
<script src="jquery.js"> </script>
<script src="jquery-ui.js"> </script>
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
 <style>

			
 
 
 
 
 </style>
  </head>
 
 
 <body>
   <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
   <div class="navbar-header"> 
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collpase">
		 <span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		<span class="icon-bar"> </span>
		</button>
    
      
	  </div> <!-- End navbar header -->
	  <?php
  session_start();
	if(!empty($_SESSION['SID']))
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
		echo "<li><a href='userprofile.php'>Welcome ".$_SESSION['SID']."</a></li>";
		echo  "<li><a  href='logout.php'><span class='glyphicon glyphicon-log-in'></span> Logout</a></li>";
		echo '</ul>';
		echo '</div>';
	}
	else
	{
		echo '<div class="collapse navbar-collapse" id=""> ';
		echo '<a href="index.php"> <img class="img" src="http://presidencycollege.ac.in/wp-content/themes/website/svg/presidency_logo.svg"></a>';
		echo '<ul class="nav navbar-nav navbar-right">';
	
		echo '</ul>';
		echo '</div>';
	}
	?>
  
    
  </div>
</nav> <!-- Navbar End-->


 
  <header class="navbar ">
  <div class="container container-fluid">
  <div class="row">
	<div class=" col-sm-3">
	<a  class="navbar-brand glyphicon glyphicon-home" style="color:white;font-size:20px;" href="index.php"> Home</a>
	</div>
	<div class="col-md-3">
     <ul class="nav navbar-nav " >
      
      <li class="dropdown">
        <a class="dropdown-toggle mnuclr" data-toggle="dropdown">Course <span class="caret"></span></a>
		
        <ul class="dropdown-menu ">
          <li><a href="mba.php">MBA</a></li>
          <li><a href="mca.php">MCA</a></li>
        </ul>
		</li>
		</div>
	  
	 
	  <div class="col-md-3 nav navbar-nav">
      <li> <a class="mnuclr" href="about.php">About </a></li>
	  </div>
	  
	  
	  <div class="col-md-3 nav navbar-nav">
      <li><a  class="mnuclr" href="contact.php">Contact us</a></li>
	  </div>
	 
    </ul>
	</div>
  </div>
  </header>
  
   <div class="well">
    <h2>MCA Course Details</h2>
  </div>
  
  
  <div class="container">
  <div class=" col-md-4">
  <div class="thumbnail ">
  <img src="images/future-online-ed-banner-2.png" alt="img">
  </div>
  </div>
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/education-banner.jpg" alt="img">
  </div>
  </div>
  
  <div class=" col-md-4">
  <div class="thumbnail">
  <img src="images/highered-banner1.jpg" alt="img">
  </div>
  </div>
  
  </div>
  <div class="jumbotron container">
  <div class="row">
		<div class="col-md-6 ">
		 <img style="float:right;" class="img-thumbnail"  src="images/MCA444.jpg" alt="img">
		</div>
		<div class="col-md-6" >
		 <p style="font-size:15px"> Department of Computer Applications had its humble beginning,
		 in the year 2000, offering both BCA and MCA with a strength less than 20. Since then the
		 department had witnessed a tremendous growth which now has over 500 students and 20 staff members.
		 The department boasts of state of the art infrastructure facility and a spacious library with diverse 
		 collection of volumes for enriching knowledge.</p>
		 
		 <p style="font-size:15px">
		 The first use of the word “computer” was recorded in 1613, referring to a person who carried out
		 calculations, or computations, and the word continued with the same meaning until the middle of 
		 the 20th century. From the end of the 19th century the word began to take on its more familiar 
		 meaning, a machine that carries out computations.
		 </p>
		 <p style="font-size:15px">
		Today Computers have become an indispensable media for all the day to day activities. 
		To cater to the needs of emergent IT industry, the Department of Computer Science was 
		introduced in the year 2000.
		 </p>
		 </div>
		 
		 
		
		 </div>
		 <div class="row">
		 <div class="col-md-12" >
		 <p style="font-size:15px">It is a course that is customized for students wishing to
		 shine in the field of computers & information technology. The course aims at providing
		 sting knowledge to the students on different areas like database and application packages,
		 networking & Internet, logical & numerical methods, programming basics & computer fundamentals.
		 The course also helps the students to develop core competencies in computer applications 
		 thereby creating the best professionals for the IT industry.
		 </p>
		 
		 <p style="font-size:15px">The entire course is outcome based and faculty are sensitive to
		 ensure that the outcomes for each course is defined well at the beginning of the programme
		 and shared with each student through course hand outs. This ensures expectation setting is
		 taken care of between the learner and the teacher. Interactive lectures, pedagogy that is 
		 evolving, assignments and assessments ensure that all students achieve the specified outcomes. 
		 The role of the faculty at Presidency thus is more of a mentor, facilitator and learning is outcome based.
		 </p>
		</div>
		</div>

  </div>
  <hr/>
  </div>
  
  <div class="row ">
  <div class="container jumbotron">
  <div class="col-lg-7">
  <h5 style="font-weight:bold;"> ELIGIBILITY</h5>
  <p style="font-size:15px;">
  The eligibility under Bangalore University is a minimum of 50% (45% n case of SC/ST candidates from Karnataka only)
  aggregate in graduation from any UGC recognized University..</p>
 
  
   <h5 style="font-weight:bold;">DEPARTMENTAL ACTIVITIES</h5>
   <ul class="mbaspecialization">
	<li class=" glyphicon glyphicon-arrow-right"> Two guest (2) lectures per semester on any current issue of academic relevance supported by one (1) workshop, per semester</li><br>
	<li class="glyphicon glyphicon-arrow-right"> Industrial visits & outbound trip (at least one (1) per semester) to reinforce conceptual knowledge</li>
	<li class="glyphicon glyphicon-arrow-right"> Encouraging participation of students in various IT fests as part of the hands-on learning experience</li>
   </ul>
  </div>
    <br>
  <div class="col-md-5">
  <form action="coursereg.php" method="post">
  <input class="btn btn-warning" type="submit" value="NEW USERS CLICK HERE TO REGISTER"	>
  </form>
  <hr/>
 
  <form action="loginpage.php" method="">
  <input class="btn btn-success" type="submit" value="REGISTERD USERS CLICK HERE">
  </form>
  <hr>
  
  <form action="mca_test.php" method="">
  <input class="btn btn-primary" type="submit" value="TAKE UP A TEST">
  </form>
  <hr>
  </div>
  </div>
  <hr>
   <div class="container">
  <div class="">
  <img src="images/guidlens-steps.jpg" alt="img"class="img-thumbnail">
  </div>
  </div>
  
  <hr class="">
  
  
  
  
  <footer class="footer">
 <div class="container">
 <div class="row">
 <div class="col-md-7">
 <address class="address">
 <h5>Contact Address</h5>
 Kempapura,<br>
 Hebbal, Bengaluru,<br>
 Karnataka 560024,<br>
 Phone: 080 4247 8704
 </address>
 </div>
 </div>
 <div class="bottom">
 <div class="col-md-5"> </div>
 <div class="col-md-7"> 
	<ul class="footer-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="contact.php"> Contact</a></li>
		<li><a href="about.php"> About Us</a></li>
	</ul>
 
 </div>
 </div>
 </footer>

 
<script src="js/bootstrap.min.js"></script>
</body>
</html>
